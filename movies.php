<<?php
include_once 'init.php';

// Definition de la chaine de la requête
$query_string = "SELECT
    title,
    actors,
    director,
    producer,
    year_of_prod,
    language
 FROM movies";

// Definition de la requête pour PDO
$pdo = $pdo->query($query_string);


// PDO::FETCH_OBJ - Retourne le tableau du résultat de la requete
$auteurs = $pdo->fetchAll( PDO::FETCH_ASSOC );


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.jpg">

    <title>f</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body>
    <!-- Un menu pour habiller la page  -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">FILM</a>
        </div>

        <br><br><br>






<div class="container">

      <table class="table table-hover">

        <tr class="info">
          <th>Titre du film:</th>
          <th>acteur:</th>
          <th>Réalisateur:</th>
          <th>Producteur:</th>
          <th>Année de production:</th>
          <th>Langue:</th>
          <th>Categorie:</th>
          <th>Synopsis:</th>
          <th>Video:</th>
        </tr>

        <!-- Boucle du tableau  -->
        <?php foreach($movies as $movie): ?>
          <tr>
            <td><?php echo $movie['title']; ?></td>
            <td><?php echo $movie['actorlastname']; ?></td>
            <td><?php echo $movie['directorlastname']; ?></td>
            <td><?php echo $movie['producerlastname']; ?></td>
            <td><?php echo $movie['year']; ?></td>
            <td><?php echo $movie['langue']; ?></td>
            <td><?php echo $movie['category']; ?></td>
            <td><?php echo $movie['storyline']; ?></td>
            <td><?php echo $movie['video']; ?></td>
            <td><?php echo $movie['nom']; ?></td>
          </tr>

        <?php endforeach; ?>
      </table>

      <?php
      echo "<br>";
      echo "<a href=\"addmovies.php\">Ajouter un film</a>";
      echo "<br>";

      ?>

    </div>
