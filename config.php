<?php

// --------------------
// CONFIG DU PROGRAMME
// --------------------

// Adresse du serveur de base de données
// "localhost" est un alias de 127.0.0.1
$host = "127.0.0.1";

// Nom d'utilisateur de la base de données
$user = "root";

// Mot de passe associé à l'utilisateur (root) de la base de données
$pass = "";

// Nom de la base de données sur laquelle on va travailler
$database = "exercice_3";

// Les langues disponibles
$language = [
  "Français",
  "Anglais",
  "Espagnol",
  "Italien",
  "Portugais",
  "Arabe",
  "Russe"
];

// Les categories de film disponible

$categories = [
  "Amour ",
  "Enfant ",
  "Western ",
  "Policier",
  "Effet Speciaux",
  "Comédie ",
];
