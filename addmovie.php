<?php
include_once 'init.php';

// ------------------
// Déclaration des variables par défaut
// --
// On utilisera ces variables dans le formulaire (attribut "value" des champs)
// Les valeurs NULL seront surchargées avec les valeur saisies par l'utilisateur
// ------------------
$titre_film       = null;
$nom_acteur       = null;
$nom_realisateur  = null;
$nom_producteur   = null;
$annee_prod       = null;
$lang_dispo       = null;
$categorie        = null;
$synopsis         = null;
$video             = null;

// ------------------
// PARTIE 1
// ------------------
// ON GERE L'ENVOIS DU FORMULAIRE
// Le tableau de la "super globale" $_POST sera généré UNIQUEMENT lors de l'envois du
// formulaire avec la méthode POST (<form method="post">)
// ------------------
if (!empty($_POST)) {

  $send = true;
  // Recupération des données du formulaire
  // --

  $titre_film      = isset($_POST['title'])               ?     $_POST['title']            : null;
  $nom_acteur      = isset($_POST['actorlastname'])       ?     $_POST['actorlastname']     : null;
  $nom_realisateur = isset($_POST['directorlastname'])    ?     $_POST['directorlastname']  : null;
  $nom_producteur  = isset($_POST['producerlastname'])     ?    $_POST['producerlastname']  : null;
  $annee_prod      = isset($_POST['year'])                 ?    $_POST['year']              : null;
  $lang_dispo      = isset($_POST['langue'])                 ?   $_POST['langue']            : null;
  $categorie       = isset($_POST['category'])              ?   $_POST['category']          : null;
  $synopsis        = isset($_POST['storyline'])              ?   $_POST['storyline']         : null;
  $video           = isset($_POST['video'])                   ?  $_POST['video']            : null;



  // - Controle du titre du FILM
  // --
  // -> doit être une chaine alphabetique
  // -> peut contenir un tiret
  // -> ne doit pas possèder de caractère numérique
  if (!preg_match("/^[a-z][a-z-]*[a-z]$/i", $titre_film)) {
    $send = false;
    setFlashbag("danger", "Vérifiez la syntaxe du titre du film");
  }

  // - Controle du nom de l'acteur
  // --
  // -> doit être une chaine alphabetique
  // -> peut contenir un tiret
  // -> ne doit pas possèder de caractère numérique
  if (!preg_match("/^[a-z][a-z-]*[a-z]$/i", $nom_acteur)) {
    $send = false;
    setFlashbag("danger", "Vérifiez la syntaxe du nom de l'acteur");
  }
  // - Controle du nom du réalisateur
  // --
  // -> doit être une chaine alphabetique
  // -> peut contenir un tiret
  // -> ne doit pas possèder de caractère numérique
  if (!preg_match("/^[a-z][a-z-]*[a-z]$/i", $nom_realisateur)) {
    $send = false;
    setFlashbag("danger", "Vérifiez la syntaxe du nom du réalisateur");
  }

  // - Controle du nom du producteur
  // --
  // -> doit être une chaine alphabetique
  // -> peut contenir un tiret
  // -> ne doit pas possèder de caractère numérique
  if (!preg_match("/^[a-z][a-z-]*[a-z]$/i", $nom_producteur)) {
    $send = false;
    setFlashbag("danger", "Vérifiez la syntaxe du nom du producteur");
  }

  // - Controle l'année de production
  //
  if (($annee_prod) {
    $send = false;
    setFlashbag("danger", "Veuillez sélectionner une année de production pour ce film");
  }

    if ($send) {
    // Requête d'insertion d'un film dans la BDD
    $query_string = "INSERT INTO `movies` (`title`, `actors`, `director`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video` )
              VALUES (:title, :actors, :director, :producer, :year_of_prod, :language, :category, :storyline , :video )";

    // On demande à PDO de préparer la requête
    $pdo = $pdo->prepare($query_string);

    // On prépare les "variables" de PDO

    $pdo->bindValue(":title"               , $titre_film,        PDO::PARAM_STR);
    $pdo->bindValue(":actors"              , $nom_auteur,     PDO::PARAM_STR);
    $pdo->bindValue(":director"            , $nom_realisateur,     PDO::PARAM_STR);
    $pdo->bindValue(":producer"            , $nom_producteur,     PDO::PARAM_STR);
    $pdo->bindValue(":year_of_prod"        , $annee_prod,      PDO::PARAM_STR);
    $pdo->bindValue(":language"             , $lang_dispo,      PDO::PARAM_STR);
    $pdo->bindValue(":category"             , $category,      PDO::PARAM_STR);
    $pdo->bindValue(":storyline"            , $synopsis,      PDO::PARAM_STR);
    $pdo->bindValue(":video"               , $video,      PDO::PARAM_STR);


    // On exécute la requête
    $pdo->execute();
  }

  header("location: home.php");
  exit;
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.jpg">

    <title>f</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body>
    <!-- Un menu pour habiller la page  -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">FILM</a>
        </div>

        <br><br><br>
    <form method="post">

      <div class="container">

        <div class="form-group">
          <label for="title">Titre du film</label><br>
          <input type="text" id="title" name="title" value="<?php echo $titre_film; ?>">
        </div>

        <div class="form-group">
          <label for="actorlastname">Nom de l'acteur</label><br>
          <input type="text" id="actorlastname" name="actorlastname" value="<?php echo $nom_acteur; ?>">
        </div>

        <div class="form-group">
          <label for="directorlastname">Nom du réalisateur</label><br>
          <input type="text" id="directorlastname" name="directorlastname" value="<?php echo $nom_realisateur; ?>">
        </div>

        <div class="form-group">
          <label for="producerlastname">Nom du producteur</label><br>
          <input type="text" id="producerlastname" name="producerlastname" value="<?php echo $nom_producteur; ?>">
        </div>

        <div class="form-group">
          <label for="year">Année de production</label><br>
          <select id="year" name="year">
            <option value="">Choisir</option>
            <?php for($i=date('Y'); $i>date('Y')-100; $i--): ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php endfor; ?>
          </select>
        </div>

        <div class="form-group">
          <select name="langue">
                <option value="">Langues</option>
                <?php for($i=0; $i<7; $i++): ?>
                  <option value="<?php echo $language[$i]; ?>"><?php echo $language[$i]; ?></option>
                <?php endfor; ?>
          </select>
        </div>

        <div class="form-group">
          <select name="category">
                <option value="">Catégories</option>
                <?php for($i=0; $i<6; $i++): ?>
                  <option value="<?php echo $language[$i]; ?>"><?php echo $language[$i]; ?></option>
                <?php endfor; ?>
          </select>
        </div>


        <div class="form-group">
          <label for="storyline">Synopsis</label><br>
          <input type="text" id="storyline" name="storyline" value="<?php echo $synopsis; ?>">
        </div>

        <div class="form-group">
          <label for="video">video</label><br>
          <input type="text" id="video" name="video" value="<?php echo $video; ?>">
        </div>

          <button class="btn btn-primary" type="submit">Ajouter ce film</button>
        </div>
      </form>


  </body>
</html>
