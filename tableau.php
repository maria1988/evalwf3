<?php
// --------
// EXERCICE 1  : On se presente !
// --------

// 1. Tableau de presentation en langage Php

$array =  array(
  "Prénom" => "Mariama",
  "Nom" => "Fadiga",
  "Adresse" => "22 rue République",
  "Code Postal" => "02100",
  "Ville" => "Lille",
  "Email"  => "fadiga.mariama@gmail.com",
  "Telephone"=> "00.00.00.00.00",
  "Date_de_naissance" => "1988-07-31"
);

// 2. Affichage de la date $newDate au format ("d-m-Y")

$date_de_naissance = $array['Date_de_naissance'];
$newDate = DateTime::createFromFormat("Y-m-d", $date_de_naissance);
$newDate->format('d-m-Y');
   //   Condition
if ($array['Date_de_naissance'] != $newDate) {
   echo $newDate;
  // Affichage de la date
$newDate = new DateTime($array['Date_de_naissance']);
echo $newDate->format('d-m-Y');

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Tableau et liste de Présentation </title>
    </head>

    <body>
      <h1> Liste de présentation</h1>
      <div class="container">
        <ul>
          <!-- Boucle du tableau des informations dans une liste HTML -->
          <?php foreach ($array as $key => $value) : ?>
              <li>
                  <?php if( $key == "Date_de_naissance") {
                              $newDatedate = new DateTime($value);
                              echo $key." : ". $newdate->format('d/m/Y');
                        } else {
                              echo $key." : ". $value;
                        } ?>
              </li>
          <?php endforeach; ?> <!-- fin de la boucle -->
        </ul>

      </div>
    </body>
</html>
